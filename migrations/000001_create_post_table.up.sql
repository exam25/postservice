CREATE TABLE IF NOT EXISTS post(
    post_id serial PRIMARY KEY NOT NULL,
    name text not null,
    description text not null,
    customer_id INT not NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),deleted_at TIMESTAMP);
