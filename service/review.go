package service

import (
	"context"
	pbr "github/FIrstService/exammicro/postservice/genproto/review"
	l "github/FIrstService/exammicro/postservice/pkg/logger"
	grpcclient "github/FIrstService/exammicro/postservice/service/grpc_client"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ReviewService ...
type ReviewService struct {
	review *grpcclient.ServiceManager
	logger l.Logger
}

func (s *ReviewService) CreateReview(ctx context.Context, req *pbr.ReviewReq) (*pbr.ReviewResp, error) {
	review, err := s.review.ReviewService().CreateReview(ctx, req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert user", err))
		return &pbr.ReviewResp{}, status.Error(codes.Internal, "something went wrong, please check review info")
	}
	return review, nil
}

func (s *ReviewService) GetReviewById(ctx context.Context, req *pbr.ID) (*pbr.Review, error) {
	_, err := s.review.ReviewService().GetReviewById(ctx, req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error select users", err))
		return &pbr.Review{}, status.Error(codes.Internal, "something went wrong, please check reviews info")
	}
	return &pbr.Review{}, nil
}

func (s *ReviewService) UpdateReview(ctx context.Context, req *pbr.Review) (*pbr.Review, error) {
	res, err := s.review.ReviewService().UpdateReview(ctx, req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pbr.Review{}, status.Error(codes.InvalidArgument, "Please check reviews info")
	}
	return res, nil

}

func (s *ReviewService) DeleteByPostId(ctx context.Context, req *pbr.ID) (*pbr.Empty, error) {
	_, err := s.review.ReviewService().DeleteByPostId(ctx, req)
	if err != nil {
		s.logger.Error("Error while delete post", l.Any("Delete", err))
		return &pbr.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	return &pbr.Empty{}, nil
}
