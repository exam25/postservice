package service

import (
	"context"
	"fmt"
	pb "github/FIrstService/exammicro/postservice/genproto/customer"
	pbp "github/FIrstService/exammicro/postservice/genproto/post"
	pbr "github/FIrstService/exammicro/postservice/genproto/review"
	l "github/FIrstService/exammicro/postservice/pkg/logger"
	grpcclient "github/FIrstService/exammicro/postservice/service/grpc_client"
	"github/FIrstService/exammicro/postservice/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// PostService ...
type PostService struct {
	review *grpcclient.ServiceManager
	// customer *grpcclient.ServiceManager
	storage storage.IStorage
	logger  l.Logger
}

// NewPostService ...

func NewPostService(review *grpcclient.ServiceManager, db *sqlx.DB, log l.Logger) *PostService {
	return &PostService{
		review:  review,
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *PostService) CreatePost(ctx context.Context, req *pbp.PostReq) (*pbp.PostResp, error) {
	post, err := s.storage.Post().CreatePost(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert post", err))
		return &pbp.PostResp{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	return post, nil
}

func (s *PostService) GetPostById(ctx context.Context, req *pbp.ID) (*pbp.GetPostInfo, error) {
	res, err := s.storage.Post().GetPostById(req)
	if err != nil {
		s.logger.Error("Error while getting", l.Any("error select post", err))
		return &pbp.GetPostInfo{}, status.Error(codes.Internal, "something went wrong,please check your post id")
	}
	response := &pbp.GetPostInfo{
		PostId:      res.PostId,
		Description: res.Description,
		Name:        res.Name,
		Medias:      res.Medias,
	}
	id := &pbr.ID{Id: response.PostId}
	rewiews, err := s.review.ReviewService().GetByPostId(ctx, id)
	if err != nil {
		s.logger.Error("Error while getting post rewiews", l.Error(err))
		// rewiews = &pbr.GetRewiewsRes{}
		return &pbp.GetPostInfo{}, err
	}
	for _, rew := range rewiews.Rewiews {
		response.Rewiews = append(response.Rewiews, &pbp.Review{
			Id:          rew.Id,
			PostId:      rew.PostId,
			CustomerId:  rew.CustomerId,
			Description: rew.Description,
			Rating:      rew.Rating,
		})
	}
	response.AvarageRewiew = uint32(rewiews.Rating)
	user, err := s.review.CustomerService().GetCustomerById(ctx, &pb.ID{
		Id: res.CustomerId,
	})
	if err != nil {
		s.logger.Error("Error while getting post customer", l.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't get the post customer")
	}

	tempUser := &pbp.Customer{
		CustomerId:  user.CustomerId,
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Bio:         user.Bio,
		Email:       user.Email,
		PhoneNumber: user.PhoneNumber,
	}
	for _, adr := range user.Addresses {
		tempUser.Addresses = append(tempUser.Addresses, &pbp.Address{
			CustomerId: user.CustomerId,
			District:   adr.District,
			Street:     adr.Street,
		})
	}
	response.Customer = tempUser

	return response, nil
}

func (s *PostService) UpdatePost(ctx context.Context, req *pbp.Post) (*pbp.Post, error) {
	res, err := s.storage.Post().UpdatePost(req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pbp.Post{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}

func (s *PostService) DeleteDatabase(ctx context.Context, req *pbp.ID) (*pbp.Empty, error) {
	_, err := s.storage.Post().DeleteDatabase(req)
	if err != nil {
		s.logger.Error("Error while delete post", l.Any("Delete", err))
		return &pbp.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	_, err = s.review.ReviewService().DeleteByPostId(ctx, &pbr.ID{Id: req.Id})
	if err != nil {
		s.logger.Error("Error while Deleting review by post id", l.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your review")
	}

	return &pbp.Empty{}, nil
}

func (s *PostService) DeleteByCustId(ctx context.Context, req *pbp.ID) (*pbp.Empty, error) {
	fmt.Println(req)
	err := s.storage.Post().DeleteByCustID(req)
	if err != nil {
		s.logger.Error("Error while Deleting post by owner id", l.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	_, err = s.review.ReviewService().DeleteByCustomerId(ctx, &pbr.ID{Id: req.Id})
	if err != nil {
		s.logger.Error("Error while Deleting review by custom id")
		return nil, status.Error(codes.Internal, "Couldn`t delete review")
	}

	return &pbp.Empty{}, nil
}

func (s *PostService) GetByCustId(ctx context.Context, req *pbp.ID) (*pbp.GetPosts, error) {
	res, err := s.storage.Post().GetByCustId(req)
	fmt.Println(req)
	response := &pbp.GetPosts{}
	if err != nil {
		s.logger.Error("Error while getting posts", l.Error(err))
		return &pbp.GetPosts{}, status.Error(codes.Internal, "Couldn't get the posts")
	}
	for _, r := range res {
		temp := &pbp.GetPostInfo{
			PostId:      r.PostId,
			Description: r.Description,
			Name:        r.Name,
			Medias:      r.Medias,
		}
		fmt.Println(temp.PostId)
		rewiews, err := s.review.ReviewService().GetByPostId(ctx, &pbr.ID{Id: temp.PostId})
		if err != nil {
			s.logger.Error("Error while getting post rewiews", l.Error(err))
			return nil, status.Error(codes.Internal, "Couldn't get the reviews")
		}
		for _, rew := range rewiews.Rewiews {
			temp.Rewiews = append(temp.Rewiews, &pbp.Review{
				Id:          rew.Id,
				PostId:      rew.PostId,
				CustomerId:  rew.CustomerId,
				Description: rew.Description,
				Rating:      rew.Rating,
			})
		}
		temp.AvarageRewiew = uint32(rewiews.Rating)
		response.Posts = append(response.Posts, temp)
	}
	return response, nil
}

func (s *PostService) GetListPostBySearch(ctx context.Context, req *pbp.GetListBySearch) (*pbp.GetPosts, error) {
	res, err := s.storage.Post().GetListPostBySearch(req)
	if err != nil {
		s.logger.Error("Error while getting", l.Any("error select post", err))
		return &pbp.GetPosts{}, status.Error(codes.Internal, "something went wrong,please check your post id")
	}
	response := &pbp.GetPosts{}
	for _, post := range res {
		temp := &pbp.GetPostInfo{
			Name:        post.Name,
			Description: post.Description,
			PostId:      post.PostId,
		}
		for _, m := range temp.Medias {
			r := &pbp.MediasResp{
				Id:     m.Id,
				PostId: post.PostId,
				Name:   m.Name,
				Link:   m.Link,
				Type:   m.Type,
			}
			temp.Medias = append(temp.Medias, r)
		}

		customer, err := s.review.CustomerService().GetCustomerById(ctx, &pb.ID{Id: temp.Customer.CustomerId})
		if err != nil {
			s.logger.Error("Error while getting post customer", l.Error(err))
			return nil, status.Error(codes.Internal, "Couldn't get customers")
		}
		tempCustomer := &pbp.Customer{
			CustomerId:  customer.CustomerId,
			FirstName:   customer.FirstName,
			LastName:    customer.LastName,
			Bio:         customer.Bio,
			Email:       customer.Email,
			PhoneNumber: customer.PhoneNumber,
		}
		for _, address := range customer.Addresses {
			tempCustomer.Addresses = append(tempCustomer.Addresses, &pbp.Address{
				Id:         address.Id,
				CustomerId: customer.CustomerId,
				District:   address.District,
				Street:     address.Street,
			})
		}
		temp.Customer = tempCustomer
		rewiews, err := s.review.ReviewService().GetByPostId(ctx, &pbr.ID{Id: temp.PostId})
		if err != nil {
			s.logger.Error("Error while getting post rewiews", l.Error(err))
			return nil, status.Error(codes.Internal, "Couldn't get the reviews")
		}

		for _, rew := range temp.Rewiews {
			r := &pbp.Review{
				Id:          rew.Id,
				PostId:      rew.PostId,
				CustomerId:  rew.CustomerId,
				Description: rew.Description,
				Rating:      rew.Rating,
			}
			temp.Rewiews = append(temp.Rewiews, r)
		}

		temp.AvarageRewiew = uint32(rewiews.Rating)

		if err != nil {
			s.logger.Error("Error while getting post customer", l.Error(err))
			return nil, status.Error(codes.Internal, "Couldn't get the post customer")

		}
		response.Posts = append(response.Posts, temp)
		response.Count = int64(len(res))
	}
	return response, nil

}
