package postgres

import (
	"fmt"
	pbp "github/FIrstService/exammicro/postservice/genproto/post"

	"log"

	"github.com/jmoiron/sqlx"
)

type postRepo struct {
	db *sqlx.DB
}

// NewPostRepo ...

func NewPostRepo(db *sqlx.DB) *postRepo {
	return &postRepo{db: db}
}

func (r *postRepo) CreatePost(post *pbp.PostReq) (*pbp.PostResp, error) {
	postResp := pbp.PostResp{}
	err := r.db.QueryRow(`insert into post (name,description,customer_id) 
	values ($1,$2,$3) 
	returning post_id,name,description,customer_id`, post.Name, post.Description,
		post.CustomerId).Scan(&postResp.PostId, &postResp.Name, &postResp.Description, &postResp.CustomerId)
	if err != nil {
		return &pbp.PostResp{}, err
	}
	med := []*pbp.MediasResp{}
	for _, media := range post.Medias {
		media, err := r.CreatMedia(media)
		if err != nil {
			return nil, err
		}
		media.PostId = postResp.PostId
		med = append(med, media)
	}
	postResp.Medias = med

	return &postResp, nil
}

func (r *postRepo) GetPostById(ids *pbp.ID) (*pbp.Post, error) {
	tempPost := &pbp.Post{}
	err := r.db.QueryRow(`select name,
	description,
	customer_id,
	post_id from post 
	where post_id=$1 and deleted_at is null`,
		ids.Id).Scan(
		&tempPost.Name,
		&tempPost.Description, &tempPost.CustomerId, &tempPost.PostId)
	if err != nil {
		log.Fatal("Error while select owners", err)
		return &pbp.Post{}, err
	}
	tempPost.Medias, err = r.GetMediasByPostId(&pbp.ID{Id: tempPost.PostId})
	fmt.Println(tempPost)
	if err != nil {
		return &pbp.Post{}, err
	}
	return tempPost, nil
}

func (r *postRepo) UpdatePost(post *pbp.Post) (*pbp.Post, error) {
	postResp := &pbp.Post{}
	err := r.db.QueryRow(`UPDATE post SET updated_at=NOW(),name=$1,
	description=$2,customer_id=$3 
	where post_id=$4 and deleted_at is null returning post_id,name,description,customer_id`,
		post.Name, post.Description,
		post.CustomerId, post.PostId).Scan(
		&postResp.PostId,
		&postResp.Name, &postResp.Description,
		&postResp.CustomerId)
	if err != nil {
		log.Fatal("Error while update post", err)
		return &pbp.Post{}, err
	}

	for _, media := range post.Medias {
		medias := pbp.MediasResp{}
		err := r.db.QueryRow(`
		UPDATE medias SET updated_at=NOW(),name=$1,link=$2,
		type=$3 where post_id=$4 and deleted_at is null 
		returning id,name,link,type`, media.Name, media.Link, media.Type, post.PostId).Scan(
			&medias.Id, &medias.Name, &medias.Link, &medias.Type)
		if err != nil {
			return &pbp.Post{}, nil
		}
		postResp.Medias = append(postResp.Medias, &medias)
	}
	return post, nil

}

func (r *postRepo) GetByCustId(req *pbp.ID) ([]*pbp.Post, error) {
	rows, err := r.db.Query(`SELECT
		name,
		description,
		customer_id,
		post_id
		FROM post
		where customer_id=$1 and deleted_at is null`, req.Id)
	if err != nil {
		return []*pbp.Post{}, err
	}
	defer rows.Close()

	response := []*pbp.Post{}

	for rows.Next() {
		temp := &pbp.Post{}
		err = rows.Scan(
			&temp.Name,
			&temp.Description,
			&temp.CustomerId,
			&temp.PostId,
		)
		if err != nil {
			return []*pbp.Post{}, err
		}
		temp.Medias, err = r.GetMediasByPostId(&pbp.ID{Id: temp.PostId})
		fmt.Println(temp)
		if err != nil {
			temp.Medias = nil
		}
		response = append(response, temp)
	}
	return response, nil
}

func (r *postRepo) DeleteDatabase(ids *pbp.ID) (*pbp.Empty, error) {
	_, err := r.db.Exec(`UPDATE post SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, ids.Id)
	if err != nil {
		log.Fatal("Error while delete post by post id", err)
		return &pbp.Empty{}, err
	}

	return &pbp.Empty{}, nil
}

func (r *postRepo) DeleteByCustID(req *pbp.ID) error {
	fmt.Println(req)
	_, err := r.db.Exec(`
		update post SET deleted_at=NOW() where customer_id=$1 and deleted_at is null
	`, req.Id)
	if err != nil {
		log.Fatal("Error while delete post by customer id", err)
		return err
	}
	return nil
}

func (r *postRepo) GetListPostBySearch(req *pbp.GetListBySearch) ([]*pbp.Post, error) {
	offset := (req.Page - 1) * req.Limit
	search := fmt.Sprintf("%s LIKE $1", req.Search.Field)
	order := fmt.Sprintf("ORDER BY %s %s", req.Orders.Field, req.Orders.Values)

	query := `SELECT 
	name,
	customer_id,
	description 
	FROM post
	where deleted_at is null` + " and " + search + " " + order + " "

	rows, err := r.db.Query(query+"LIMIT $2 OFFSET $3", "%"+req.Search.Values+"%", req.Limit, offset)
	if err != nil {
		return []*pbp.Post{}, err
	}
	defer rows.Close()
	response := []*pbp.Post{}

	for rows.Next() {
		temp := &pbp.Post{}
		err = rows.Scan(
			&temp.Name,
			&temp.Description,
			&temp.CustomerId,
		)
		if err != nil {
			return response, err
		}
		response = append(response, temp)
	}
	return response, nil
}
