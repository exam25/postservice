package postgres

import pbp "github/FIrstService/exammicro/postservice/genproto/post"

func (r *postRepo) CreatMedia(media *pbp.MediasReq) (*pbp.MediasResp, error) {
	temps := pbp.MediasResp{}
	err := r.db.QueryRow(`
			INSERT INTO medias(  
				name, 
				link, 
				type
			) 
			values($1, $2, $3)returning id,name,link,type
		`, media.Name, media.Link, media.Type).Scan(&temps.Id, &temps.Name, &temps.Link, &temps.Type)
	if err != nil {
		return &pbp.MediasResp{}, err
	}
	return &temps, nil
}

// func (r *postRepo) DeleteMedia(PostId *pbp.ID) error {
// 	return r.db.QueryRow(`update medias SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, PostId.Id).Err()
// }

func (r *postRepo) GetMediasByPostId(PostId *pbp.ID) ([]*pbp.MediasResp, error) {
	rows, err := r.db.Query(`SELECT 
	id,
	post_id, 
	name, 
	link, 
	type 
	FROM medias where post_id=$1 and deleted_at is null`, PostId.Id)
	if err != nil {
		return []*pbp.MediasResp{}, err
	}
	defer rows.Close()

	response := []*pbp.MediasResp{}
	for rows.Next() {
		temp := &pbp.MediasResp{}
		err = rows.Scan(
			&temp.Id,
			&temp.PostId,
			&temp.Name,
			&temp.Link,
			&temp.Type,
		)
		if err != nil {
			return []*pbp.MediasResp{}, err
		}
		response = append(response, temp)
	}

	return response, nil
}
