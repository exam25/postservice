package repo

import pbp "github/FIrstService/exammicro/postservice/genproto/post"

// PostStorageI ...
type PostStorageI interface {
	CreatePost(*pbp.PostReq) (*pbp.PostResp, error)
	GetPostById(*pbp.ID) (*pbp.Post, error)
	GetByCustId(*pbp.ID) ([]*pbp.Post, error)
	UpdatePost(*pbp.Post) (*pbp.Post, error)
	DeleteDatabase(*pbp.ID) (*pbp.Empty, error)
	DeleteByCustID(*pbp.ID) error
	GetListPostBySearch(*pbp.GetListBySearch) ([]*pbp.Post, error)
}
