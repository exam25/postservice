pull_submodule:
	git submodule update --init --recursive

update_submodule:
	git submodule update --remote --merge

run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:grpc-first/protos.git

run_script:
	./script/gen-proto.sh

swag:
	swag init -g ./api/router.go -o api/docs

create migrate:
	migrate create -ext sql -dir migrations -seq create_xxx_table

migrate up:
	migrate -source file://migrations/ -database postgres://azamali:azamali@database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com:5432/postdb1 up

migrate down:
	migrate -source file://migrations/ -database postgres://azam:Azam_2000@localhost:5432/exam2 down