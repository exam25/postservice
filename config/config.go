package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment         string //develop, staging, production
	PostgresHost        string
	PostgresPort        int
	PostgresDatabase    string
	PostgresUser        string
	PostgresPassword    string
	ReviewServiceHost   string
	ReviewServicePort   int
	CustomerServiceHost string
	CustomerServicePort int
	LogLevel            string
	RPCPort             string
}

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT",
		"develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST",
		"localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT",
		5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "postdb1"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "azam"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "Azam_2000"))

	c.ReviewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToInt(getOrReturnDefault("REVIEW_SERVICE_PORT", 9003))

	c.CustomerServiceHost = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToInt(getOrReturnDefault("CUSTOMER_SERVICE_PORT", 9001))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":9002"))
	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
